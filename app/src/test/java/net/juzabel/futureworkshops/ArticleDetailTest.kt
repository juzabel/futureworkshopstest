package net.juzabel.futureworkshops

import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.data.entity.ArticleDetailEntity
import net.juzabel.futureworkshops.data.executor.JobExecutor
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import net.juzabel.futureworkshops.domain.executor.PostExecutionThread
import net.juzabel.futureworkshops.domain.interactors.ArticleDetailInteractor
import net.juzabel.futureworkshops.domain.interactors.ArticleListInteractor
import net.juzabel.futureworkshops.domain.interactors.GetNameInteractor
import net.juzabel.futureworkshops.domain.interactors.RemoveNameInteractor
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import net.juzabel.futureworkshops.domain.repository.NameRepository
import net.juzabel.futureworkshops.presentation.UIThread
import net.juzabel.futureworkshops.presentation.ui.contracts.ArticleDetailContract
import net.juzabel.futureworkshops.presentation.ui.contracts.ArticleListContract
import net.juzabel.futureworkshops.presentation.ui.presenters.ArticleDetailPresenter
import net.juzabel.futureworkshops.presentation.ui.presenters.ArticleListPresenter
import net.juzabel.futureworkshops.presentation.ui.views.fragments.ArticleDetailFragment
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.junit.ClassRule


class ArticleDetailTest {

    lateinit var view: ArticleDetailContract.View
    lateinit var executionThread: ExecutionThread
    lateinit var postExecutionThread: PostExecutionThread
    lateinit var articlesRespository: ArticlesRepository

    lateinit var presenter: ArticleDetailPresenter

    lateinit var articleDetailInteractor: ArticleDetailInteractor

    @Before
    fun setUp() {
        view = Mockito.mock(ArticleDetailContract.View::class.java)!!
        executionThread = JobExecutor()
        postExecutionThread = TestExecutionThread()
        articlesRespository = Mockito.mock(ArticlesRepository::class.java)!!

        articleDetailInteractor = ArticleDetailInteractor(executionThread,
                postExecutionThread, articlesRespository)

        presenter = ArticleDetailPresenter(articleDetailInteractor)

        presenter.view = view
    }

    @Test
    @Throws(Exception::class)
    fun useAppContext() {

        var articleDetail : ArticleDetail = ArticleDetail(1,"title", "imageurl", "source", "content", "date")

        `when`(articlesRespository.getArticleDetail(1)).thenReturn(

                Observable.create({
                    it.onNext(articleDetail)
                    it.onComplete()
                })

        )

        presenter.create()
        presenter.getArticleDetail(1)

        Mockito.verify(view, Mockito.timeout(2000).times(1)).articleDetailGot(articleDetail)

    }

    class TestExecutionThread : PostExecutionThread {

        override val scheduler: Scheduler
            get() = Schedulers.trampoline()
    }
}