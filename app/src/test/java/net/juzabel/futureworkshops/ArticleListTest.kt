package net.juzabel.futureworkshops

import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.juzabel.futureworkshops.data.executor.JobExecutor
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import net.juzabel.futureworkshops.domain.executor.PostExecutionThread
import net.juzabel.futureworkshops.domain.interactors.ArticleListInteractor
import net.juzabel.futureworkshops.domain.interactors.GetNameInteractor
import net.juzabel.futureworkshops.domain.interactors.RemoveNameInteractor
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import net.juzabel.futureworkshops.domain.repository.NameRepository
import net.juzabel.futureworkshops.presentation.UIThread
import net.juzabel.futureworkshops.presentation.ui.contracts.ArticleListContract
import net.juzabel.futureworkshops.presentation.ui.presenters.ArticleDetailPresenter
import net.juzabel.futureworkshops.presentation.ui.presenters.ArticleListPresenter
import net.juzabel.futureworkshops.presentation.ui.views.fragments.ArticleDetailFragment
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.junit.ClassRule



class ArticleListTest {

     lateinit var view: ArticleListContract.View
     lateinit var executionThread: ExecutionThread
     lateinit var postExecutionThread: PostExecutionThread
     lateinit var articlesRespository: ArticlesRepository
     lateinit var nameRepository: NameRepository

    lateinit var presenter: ArticleListPresenter

    lateinit var articleListInteractor: ArticleListInteractor
    lateinit var getNameInteractor: GetNameInteractor
    lateinit var removeNameInteractor: RemoveNameInteractor

    @Before
    fun setUp() {
        view =  Mockito.mock(ArticleListContract.View::class.java)!!
        executionThread = JobExecutor()
        postExecutionThread = TestExecutionThread()
        articlesRespository =  Mockito.mock(ArticlesRepository::class.java)!!
        nameRepository =  Mockito.mock(NameRepository::class.java)!!

        articleListInteractor = ArticleListInteractor(executionThread,
                postExecutionThread, articlesRespository)
        getNameInteractor = GetNameInteractor(executionThread,
                postExecutionThread, nameRepository)
        removeNameInteractor = RemoveNameInteractor(executionThread,
                postExecutionThread, nameRepository)

        presenter = ArticleListPresenter(articleListInteractor, getNameInteractor, removeNameInteractor)

        presenter.view = view
    }

    @Test
    @Throws(Exception::class)
    fun useAppContext() {
        var listArticles = ArrayList<Article>()

        listArticles.add(Article(0,
                "title_test_0",
                "icon_url_0",
                "summary_0",
                "10/10/1010",
                "content_0"))
        listArticles.add(Article(1,
                "title_test_1",
                "icon_url_1",
                "summary_1",
                "10/10/1010",
                "content_1"))

        `when`(nameRepository.getName()).thenReturn(
                Observable.create({
                    it.onNext("Test Name")
                    it.onComplete()
                })
        )

        `when`(articlesRespository.getArticles()).thenReturn(

                Observable.create({
                    it.onNext(listArticles)
                    it.onComplete()
                })

        )

        presenter.create()
        presenter.getArticlesList()
        presenter.getName()

        Mockito.verify(view, Mockito.timeout(2000).times(1)).nameGot("Test Name")
        Mockito.verify(view, Mockito.timeout(2000).times(1)).articlesGot(listArticles)

    }

    class TestExecutionThread : PostExecutionThread {

        override val scheduler: Scheduler
            get() = Schedulers.trampoline()
    }
}