package net.juzabel.futureworkshops.data.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.rx2.structure.BaseRXModel
import net.juzabel.futureworkshops.data.cache.DbHelper

/**
 * Created by julian on 31/07/17.
 *
 * Entity of a user name
 *
 */
@Table(database = DbHelper::class)
data class NameEntity(

        @Column(name = "name")
        @SerializedName("name")
        @PrimaryKey(autoincrement = false)
        @Expose
        var name: String? = null

) : BaseRXModel(), Parcelable {
        constructor(parcel: Parcel) : this(parcel.readString())

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(name)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<NameEntity> {
                override fun createFromParcel(parcel: Parcel): NameEntity {
                        return NameEntity(parcel)
                }

                override fun newArray(size: Int): Array<NameEntity?> {
                        return arrayOfNulls(size)
                }
        }
}



