package net.juzabel.futureworkshops.data.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
/**
 * Created by julian on 30/07/17.
 *
 * Entity container of articles list
 *
 */
data class ArticlesEntity(

        @SerializedName("articles")
        @Expose
        var articles :  List<ArticleEntity>

) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.createTypedArrayList(ArticleEntity.CREATOR))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(articles)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArticlesEntity> {
        override fun createFromParcel(parcel: Parcel): ArticlesEntity {
            return ArticlesEntity(parcel)
        }

        override fun newArray(size: Int): Array<ArticlesEntity?> {
            return arrayOfNulls(size)
        }
    }
}