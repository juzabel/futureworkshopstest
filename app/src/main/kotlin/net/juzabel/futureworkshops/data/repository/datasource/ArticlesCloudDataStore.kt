package net.juzabel.futureworkshops.data.repository.datasource

import io.reactivex.Observable
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.data.entity.ArticleDetailEntity
import net.juzabel.futureworkshops.data.entity.ArticleEntity
import net.juzabel.futureworkshops.data.entity.ArticlesEntity
import net.juzabel.futureworkshops.data.network.RestService
import net.juzabel.futureworkshops.domain.Article

/**
 * Created by julian on 30/07/17.
 */
class ArticlesCloudDataStore(private val restService: RestService) : ArticlesDataStore {

    override fun articleList(): Observable<ArticlesEntity>
            = restService.getArticlesList()

    override fun articleDetail(id: Long): Observable<ArticleDetailEntity>
            = restService.getArticleDetail(id)

    override fun add(articleDetail: ArticleDetail): Observable<ArticleDetailEntity>
            = throw UnsupportedOperationException()

    override fun add(article: ArticleEntity): Observable<ArticleEntity>
            = throw UnsupportedOperationException()

    override fun add(articleDetail: ArticleDetailEntity): Observable<ArticleDetailEntity>
            = throw UnsupportedOperationException()

    override fun add(article: Article): Observable<ArticleEntity>
            = throw UnsupportedOperationException()

}