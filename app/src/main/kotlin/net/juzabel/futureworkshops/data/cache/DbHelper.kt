package net.juzabel.futureworkshops.data.cache

import com.raizlabs.android.dbflow.annotation.Database
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.structure.ModelAdapter
import javax.inject.Inject


/**
 * Created by julian on 30/07/17.
 * DBFlow DBHelper class
 */
@Database(name = DbHelper.NAME, version = DbHelper.VERSION)
class DbHelper @Inject constructor(){

    companion object {
        const val NAME = "FutureWorkshopsDB"

        const val VERSION = 1
    }

}