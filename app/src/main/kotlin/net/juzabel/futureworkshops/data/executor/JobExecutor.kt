package net.juzabel.futureworkshops.data.executor

import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import java.util.concurrent.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 31/7/17.
 *
 * Pool of jobs (Rx)
 *
 */
@Singleton
class JobExecutor @Inject constructor() : ExecutionThread {

    private val threadPoolExecutor : ThreadPoolExecutor
    private val workQueue: BlockingQueue<Runnable>
    private val threadFactory: ThreadFactory


    init {
        this.workQueue = LinkedBlockingQueue<Runnable>()
        this.threadFactory = JobThreadFactory()
        threadPoolExecutor = ThreadPoolExecutor(START_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT,
                 workQueue, threadFactory)
    }

    override fun execute(runnable: Runnable) {
        threadPoolExecutor.execute(runnable)
    }

    private class JobThreadFactory : ThreadFactory {
        private var count = 0

        override fun newThread(runnable: Runnable): Thread {
            return Thread(runnable, THREAD_NAME + count++)
        }

        companion object {
            private val THREAD_NAME = "futureworkshops_"
        }
    }

    companion object {

        private val START_POOL_SIZE : Int = 3
        private val MAX_POOL_SIZE : Int = 5

        private val KEEP_ALIVE_TIME : Long = 10

        private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS
    }
}