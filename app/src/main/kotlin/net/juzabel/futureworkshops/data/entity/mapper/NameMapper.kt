package net.juzabel.futureworkshops.data.entity.mapper

import net.juzabel.futureworkshops.data.entity.NameEntity
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by julian on 31/07/17.
 *
 * This class converts between layer entities
 *
 */
@Singleton
class NameMapper @Inject constructor() {

    /**
     * It transforms an NameEntity into String
     *
     * @property nameEntity NameEntity to convert
     *
     * @return String converted
     */
    fun transform(nameEntity: NameEntity): String {
        return nameEntity.name!!
    }
}