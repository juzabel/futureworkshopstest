package net.juzabel.futureworkshops.data.network

import com.google.gson.Gson
import io.reactivex.Observable
import net.juzabel.futureworkshops.BuildConfig
import net.juzabel.futureworkshops.data.entity.ArticleDetailEntity
import net.juzabel.futureworkshops.data.entity.ArticlesEntity
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 30/7/17.
 *
 * Retrofit service creation
 *
 */
@Singleton
class RestService @Inject constructor() {

    private val service: Service

    init {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder: OkHttpClient.Builder = OkHttpClient.Builder().addInterceptor(interceptor)

        val retrofit: Retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build()).build()
        service = retrofit.create(Service::class.java)


    }

    fun getArticlesList() : Observable<ArticlesEntity> =service.getArticleList()

    fun getArticleDetail(id : Long) : Observable<ArticleDetailEntity> =service.getArticleDetail(id)

}