package net.juzabel.futureworkshops.data.repository.datasource

import com.raizlabs.android.dbflow.kotlinextensions.delete
import com.raizlabs.android.dbflow.kotlinextensions.save
import com.raizlabs.android.dbflow.rx2.language.RXSQLite
import com.raizlabs.android.dbflow.sql.language.SQLite
import io.reactivex.Observable
import io.reactivex.rxkotlin.toSingle
import net.juzabel.futureworkshops.data.cache.DbHelper
import net.juzabel.futureworkshops.data.entity.*
import net.juzabel.futureworkshops.domain.Article


/**
 * Created by juzabel on 30/7/17.
 */
class NameDBDataStore(private val dbHelper: DbHelper) : NameDataStore {
    override fun getName(): Observable<NameEntity>
            = RXSQLite.rx<NameEntity>(
            SQLite.select()
                    .from<NameEntity>(NameEntity::class.java))
            .querySingle().map { if (it == null) NameEntity() else it }.toObservable()


    override fun setName(name: String): Observable<NameEntity> {

        var nameEntity: NameEntity = NameEntity(name)

        removeName().subscribe()

        return nameEntity.insert().map {
            nameEntity
        }.toObservable()
    }

    override fun removeName(): Observable<NameEntity> {
        return RXSQLite.rx<NameEntity>(
                SQLite.select()
                        .from<NameEntity>(NameEntity::class.java))
                        .querySingle().map {
                            it.delete().subscribe()
                            it
                         }.toObservable()
    }


}