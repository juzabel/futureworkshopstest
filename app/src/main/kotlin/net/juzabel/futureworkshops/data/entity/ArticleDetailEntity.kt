package net.juzabel.futureworkshops.data.entity

import android.os.Parcel
import android.os.Parcelable
import com.raizlabs.android.dbflow.annotation.Table
import net.juzabel.futureworkshops.data.cache.DbHelper
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.rx2.structure.BaseRXModel


/**
 * Created by julian on 30/07/17.
 *
 * Entity of an Article detail
 *
 */
@Table(database = DbHelper::class)
data class ArticleDetailEntity(

        @Column(name = "id")
        @PrimaryKey(autoincrement = false)
        @SerializedName("id")
        @Expose
        var id: Long = 0,

        @Column(name = "title")
        @SerializedName("title")
        @Expose
        var title: String? = null,

        @Column(name = "image_url")
        @SerializedName("image_url")
        @Expose
        var imageUrl: String? = null,

        @Column(name = "source")
        @SerializedName("source")
        @Expose
        var source: String? = null,

        @Column(name = "content")
        @SerializedName("content")
        @Expose
        var content: String? = null,

        @Column(name = "date")
        @SerializedName("date")
        @Expose
        var date: String? = null

) : BaseRXModel(), Parcelable {
        companion object {
                @JvmField val CREATOR: Parcelable.Creator<ArticleDetailEntity> = object : Parcelable.Creator<ArticleDetailEntity> {
                        override fun createFromParcel(source: Parcel): ArticleDetailEntity = ArticleDetailEntity(source)
                        override fun newArray(size: Int): Array<ArticleDetailEntity?> = arrayOfNulls(size)
                }
        }

        constructor(source: Parcel) : this(
        source.readLong(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeLong(id)
                dest.writeString(title)
                dest.writeString(imageUrl)
                dest.writeString(source)
                dest.writeString(content)
                dest.writeString(date)
        }
}