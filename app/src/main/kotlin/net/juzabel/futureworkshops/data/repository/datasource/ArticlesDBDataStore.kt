package net.juzabel.futureworkshops.data.repository.datasource

import com.raizlabs.android.dbflow.rx2.language.RXSQLite
import com.raizlabs.android.dbflow.sql.language.SQLite
import io.reactivex.Observable
import net.juzabel.futureworkshops.data.cache.DbHelper
import net.juzabel.futureworkshops.data.entity.*
import net.juzabel.futureworkshops.domain.Article


/**
 * Created by juzabel on 30/7/17.
 */
class ArticlesDBDataStore(private val dbHelper: DbHelper) : ArticlesDataStore {

    /**
     * Obtains all the saved articles
     *
     * @return Observable<ArticlesEntity>
     */
    override fun articleList(): Observable<ArticlesEntity>
            = RXSQLite.rx<ArticleEntity>(
            SQLite.select()
                    .from<ArticleEntity>(ArticleEntity::class.java))
            .queryList().map {
        ArticlesEntity(it)
    }.toObservable()


    /**
     * Obtains an article detail
     *
     * @param id Article identifier
     *
     * @return Observable<ArticleDetailEntity>
     */
    override fun articleDetail(id: Long): Observable<ArticleDetailEntity>
            = RXSQLite.rx<ArticleDetailEntity>(
            SQLite.select()
                    .from<ArticleDetailEntity>(ArticleDetailEntity::class.java)
                    .where(ArticleDetailEntity_Table.id.eq(id)))
            .querySingle().toObservable()

    /**
     * Adds an article and returns the inserted object
     *
     * @param article Article to insert
     *
     * @return Observable<ArticleEntity>
     *
     */
    override fun add(article: Article): Observable<ArticleEntity> {

        var articleEntity: ArticleEntity
                = ArticleEntity(article.id, article.title, article.iconUrl, article.summary, article.date, article.content)

        return add(articleEntity)

    }

    /**
     * Adds an articleEntity and returns the inserted object
     *
     * @param article ArticleEntity to insert
     *
     * @return Observable<ArticleEntity>
     */
    override fun add(article: ArticleEntity): Observable<ArticleEntity> {
        return article.exists().map {
            if (it) {//if exists = Update
                article.update().map {
                    article
                }.blockingGet()
            } else { //if not = Insert
                article.insert().map {
                    article
                }.blockingGet()
            }
        }.toObservable()
    }

    /**
     * Adds an articleDetail and returns the inserted object
     *
     * @param articleDetail ArticleDetail to insert
     *
     * @return Observable<ArticleDetailEntity>
     */
    override fun add(articleDetail: ArticleDetail): Observable<ArticleDetailEntity> {

        var articleDetailEntity: ArticleDetailEntity
                = ArticleDetailEntity(articleDetail.id, articleDetail.title, articleDetail.imageUrl,
                articleDetail.source, articleDetail.content, articleDetail.date)

        return add(articleDetailEntity)

    }

    /**
     * Adds an articleDetailEntity and returns the inserted object
     *
     * @param articleDetail ArticleDetailEntity to insert
     *
     * @return Observable<ArticleDetailEntity>
     */
    override fun add(articleDetail: ArticleDetailEntity): Observable<ArticleDetailEntity> {
        return articleDetail.exists().map {
            if (it) {//if exists = Update
                articleDetail.update().map {
                    articleDetail
                }.blockingGet()
            } else { //if not = Insert
                articleDetail.insert().map {
                    articleDetail
                }.blockingGet()
            }
        }.toObservable()
    }

}