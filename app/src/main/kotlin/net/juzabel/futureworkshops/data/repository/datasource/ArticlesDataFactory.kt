package net.juzabel.futureworkshops.data.repository.datasource

import net.juzabel.futureworkshops.data.cache.DbHelper
import net.juzabel.futureworkshops.data.network.RestService
import java.lang.UnsupportedOperationException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 30/7/17.
 */
@Singleton
class ArticlesDataFactory @Inject constructor(val dbHelper: DbHelper, val restService: RestService) {

    fun createCloudDataStore(): ArticlesDataStore = ArticlesCloudDataStore(restService)

    fun createDBDataStore(): ArticlesDataStore = ArticlesDBDataStore(dbHelper)

}