package net.juzabel.futureworkshops.data.repository.datasource

import io.reactivex.Observable
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.data.entity.ArticleDetailEntity
import net.juzabel.futureworkshops.data.entity.ArticleEntity
import net.juzabel.futureworkshops.data.entity.ArticlesEntity
import net.juzabel.futureworkshops.domain.Article

/**
 * Created by juzabel on 30/7/17.
 */
interface ArticlesDataStore {
    fun articleList(): Observable<ArticlesEntity>
    fun articleDetail(id : Long): Observable<ArticleDetailEntity>
    fun add(article : Article) : Observable<ArticleEntity>
    fun add(articleDetail: ArticleDetail) : Observable<ArticleDetailEntity>
    fun add(article : ArticleEntity) : Observable<ArticleEntity>
    fun add(articleDetail: ArticleDetailEntity) : Observable<ArticleDetailEntity>
}