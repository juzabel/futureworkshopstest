package net.juzabel.futureworkshops.data.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.rx2.structure.BaseRXModel
import com.raizlabs.android.dbflow.structure.BaseModel
import net.juzabel.futureworkshops.data.cache.DbHelper

/**
 * Created by julian on 30/07/17.
 *
 * Entity of an Article
 *
 */
@Table(database = DbHelper::class)
data class ArticleEntity(

        @Column(name = "id")
        @SerializedName("id")
        @PrimaryKey(autoincrement = false)
        @Expose
        var id: Long = 0,

        @Column(name = "title")
        @SerializedName("title")
        @Expose
        var title: String? = null,

        @Column(name = "icon_url")
        @SerializedName("icon_url")
        @Expose
        var iconUrl: String? = null,

        @Column(name = "summary")
        @SerializedName("summary")
        @Expose
        var summary: String? = null,

        @Column(name = "date")
        @SerializedName("date")
        @Expose
        var date: String? = null,

        @Column(name = "content")
        @SerializedName("content")
        @Expose
        var content: String? = null

) : BaseRXModel(), Parcelable {
        companion object {
                @JvmField val CREATOR: Parcelable.Creator<ArticleEntity> = object : Parcelable.Creator<ArticleEntity> {
                        override fun createFromParcel(source: Parcel): ArticleEntity = ArticleEntity(source)
                        override fun newArray(size: Int): Array<ArticleEntity?> = arrayOfNulls(size)
                }
        }

        constructor(source: Parcel) : this(
        source.readLong(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeLong(id)
                dest.writeString(title)
                dest.writeString(iconUrl)
                dest.writeString(summary)
                dest.writeString(date)
                dest.writeString(content)
        }
}