package net.juzabel.futureworkshops.data.network

import io.reactivex.Observable
import net.juzabel.futureworkshops.data.entity.ArticleDetailEntity
import net.juzabel.futureworkshops.data.entity.ArticleEntity
import net.juzabel.futureworkshops.data.entity.ArticlesEntity
import retrofit2.http.*


/**
 * Created by juzabel on 30/7/17.
 *
 * Retrofit service interface
 *
 */
interface Service {

    @GET("fw-coding-test.json")
    fun getArticleList() : Observable<ArticlesEntity>

    @GET("{id}.json")
    fun getArticleDetail(@Path("id") id : Long): Observable<ArticleDetailEntity>

}