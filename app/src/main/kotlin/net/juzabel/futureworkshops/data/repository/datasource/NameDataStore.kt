package net.juzabel.futureworkshops.data.repository.datasource

import io.reactivex.Observable
import net.juzabel.futureworkshops.data.entity.*
import net.juzabel.futureworkshops.domain.Article

/**
 * Created by juzabel on 30/7/17.
 */
interface NameDataStore {
    fun getName(): Observable<NameEntity>
    fun setName(name : String): Observable<NameEntity>
    fun removeName() : Observable<NameEntity>
}