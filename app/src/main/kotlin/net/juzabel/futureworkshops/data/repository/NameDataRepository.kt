package net.juzabel.futureworkshops.data.repository

import io.reactivex.Observable
import io.reactivex.functions.Function
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.data.entity.ArticleEntity
import net.juzabel.futureworkshops.data.entity.mapper.ArticlesMapper
import net.juzabel.futureworkshops.data.entity.mapper.NameMapper
import net.juzabel.futureworkshops.data.repository.datasource.ArticlesDataFactory
import net.juzabel.futureworkshops.data.repository.datasource.NameDataFactory
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import net.juzabel.futureworkshops.domain.repository.NameRepository
import javax.inject.Inject

/**
 * Created by juzabel on 29/7/17.
 */
class NameDataRepository
@Inject constructor(private val mapper: NameMapper, private val dataFactory: NameDataFactory) : NameRepository {

    /**
     * Saves the username into database
     *
     * @param name Username
     *
     * @return Observable<String> Inserted object
     *
     */
    override fun setName(name: String): Observable<String>
            = dataFactory.createDBDataStore().setName(name).map {
        mapper.transform(it)
    }

    /**
     * Removes the username stored in database
     *
     * @return Observable<String> the removed item
     */
    override fun removeName(): Observable<String>
            = dataFactory.createDBDataStore().removeName().map {
        mapper.transform(it)
    }

    /**
     * Gets the stored username
     *
     * @return Observable<String> the stored item
     *
     */
    override fun getName(): Observable<String>
            = dataFactory.createDBDataStore().getName().map {
        mapper.transform(it)
    }
}