package net.juzabel.futureworkshops.data.repository

import io.reactivex.Observable
import io.reactivex.functions.Function
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.data.entity.ArticleEntity
import net.juzabel.futureworkshops.data.entity.mapper.ArticlesMapper
import net.juzabel.futureworkshops.data.repository.datasource.ArticlesDataFactory
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import javax.inject.Inject

/**
 * Created by juzabel on 30/7/17.
 */
class ArticlesDataRepository
@Inject constructor(private val mapper: ArticlesMapper, private val dataFactory: ArticlesDataFactory) : ArticlesRepository {

    /**
     * Obtains the list of articles
     *
     * @return Observable<List<Article>>
     *
     */
    override fun getArticles(): Observable<List<Article>>
            = dataFactory.createCloudDataStore().articleList().map {

        var listArticles: List<ArticleEntity> = it.articles

        for (articleEntity in listArticles) {
            dataFactory.createDBDataStore().add(articleEntity).subscribe()
        }

        mapper.transform(it.articles)

    }.onErrorResumeNext(Function {

        dataFactory.createDBDataStore().articleList().map {
            mapper.transform(it.articles)
        }

    })

    /**
     * Obtains an article detail by an id
     *
     * @param id article identifier
     *
     * @return Observable<ArticleDetail>
     */
    override fun getArticleDetail(id: Long): Observable<ArticleDetail>
            = dataFactory.createCloudDataStore().articleDetail(id).map {

        mapper.transform(it)

    }.onErrorResumeNext(Function {

        dataFactory.createDBDataStore().articleDetail(id).map {
            mapper.transform(it)
        }

    })
}