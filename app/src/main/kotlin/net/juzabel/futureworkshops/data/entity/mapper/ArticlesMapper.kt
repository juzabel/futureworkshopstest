package net.juzabel.futureworkshops.data.entity.mapper

import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.data.entity.ArticleDetailEntity
import net.juzabel.futureworkshops.data.entity.ArticleEntity
import net.juzabel.futureworkshops.domain.Article
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by julian on 31/07/17.
 *
 * This class converts between layer entities
 *
 */
@Singleton
class ArticlesMapper @Inject constructor(){

    /**
     * It transforms a data layer Article entity into a domain Article
     *
     * @property articleEntity ArticleEntity to convert
     *
     * @return Article instance
     */
    fun transform(articleEntity: ArticleEntity): Article {

        var id: Long = articleEntity.id
        var title: String? = articleEntity.title
        var iconUrl: String? = articleEntity.iconUrl
        var summary: String? = articleEntity.summary
        var date: String? = articleEntity.date
        var content: String? = articleEntity.content

        var retArticle: Article =
                    Article(id, title, iconUrl, summary, date, content)

        return retArticle

    }

    /**
     * It transforms a list of data Article entities into a list of domain Articles
     *
     * @property articleEntityList List of ArticleEntity to convert
     *
     * @return List of Article instance
     */
    fun transform(articleEntityList : List<ArticleEntity>) : List<Article> {

        var retList : ArrayList<Article> = ArrayList<Article>()

        for(articleEntity in articleEntityList){
            retList.add(transform(articleEntity))
        }

        return retList
    }


    /**
     * Transformation of article detail
     *
     * @property articleDetailEntity ArticleDetailEntity to convert
     *
     * @return ArticleDetail converted
     */
    fun transform(articleDetailEntity: ArticleDetailEntity) : ArticleDetail {
        var id: Long = articleDetailEntity.id
        var title: String? = articleDetailEntity.title
        var imageUrl: String? = articleDetailEntity.imageUrl
        var source: String? = articleDetailEntity.source
        var content: String? = articleDetailEntity.content
        var date: String? = articleDetailEntity.date

        var retArticleDetail : ArticleDetail = ArticleDetail(
                id,
                title,
                imageUrl,
                source,
                content,
                date
        )

        return retArticleDetail

    }
}