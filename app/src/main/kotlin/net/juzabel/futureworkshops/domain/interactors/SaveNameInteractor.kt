package net.juzabel.futureworkshops.domain.interactors

import io.reactivex.Observable
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import net.juzabel.futureworkshops.domain.executor.PostExecutionThread
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import net.juzabel.futureworkshops.domain.repository.NameRepository
import javax.inject.Inject

/**
 * Created by juzabel on 30/7/17.
 */
class SaveNameInteractor
@Inject constructor(threadExecutor: ExecutionThread, postExecutionThread: PostExecutionThread,
                                   private val nameRepository: NameRepository ) : UseCase<String>(threadExecutor, postExecutionThread){

    var name : String? = null

    /**
     * Sets the parameters to execute the use case
     *
     * @param name the username
     */
    fun setParameters(name: String){
        this.name = name
    }
    /**
     * Creates the use case
     *
     * @return Observable<String>
     */
    override fun buildUseCaseObservable(): Observable<String>
        = this.nameRepository.setName(name!!)



}