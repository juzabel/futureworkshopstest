package net.juzabel.futureworkshops.domain.executor

import java.util.concurrent.Executor

interface ExecutionThread : Executor