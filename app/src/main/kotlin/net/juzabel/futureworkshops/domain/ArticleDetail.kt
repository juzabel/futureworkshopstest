package net.juzabel.futureworkshops.data.entity

import android.os.Parcel
import android.os.Parcelable


/**
 * Created by julian on 30/07/17.
 *
 * Article detail class
 *
 */
data class ArticleDetail(
        var id: Long = 0,
        var title: String? = null,
        var imageUrl: String? = null,
        var source: String? = null,
        var content: String? = null,
        var date: String? = null

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeString(imageUrl)
        parcel.writeString(source)
        parcel.writeString(content)
        parcel.writeString(date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArticleDetail> {
        override fun createFromParcel(parcel: Parcel): ArticleDetail {
            return ArticleDetail(parcel)
        }

        override fun newArray(size: Int): Array<ArticleDetail?> {
            return arrayOfNulls(size)
        }
    }
}