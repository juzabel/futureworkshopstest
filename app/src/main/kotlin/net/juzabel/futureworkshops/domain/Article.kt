package net.juzabel.futureworkshops.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by julian on 30/07/17.
 *
 * Article class
 *
 */
data class Article(
        var id: Long = 0,
        var title: String? = null,
        var iconUrl: String? = null,
        var summary: String? = null,
        var date: String? = null,
        var content: String? = null

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeString(iconUrl)
        parcel.writeString(summary)
        parcel.writeString(date)
        parcel.writeString(content)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Article> {
        override fun createFromParcel(parcel: Parcel): Article {
            return Article(parcel)
        }

        override fun newArray(size: Int): Array<Article?> {
            return arrayOfNulls(size)
        }
    }
}