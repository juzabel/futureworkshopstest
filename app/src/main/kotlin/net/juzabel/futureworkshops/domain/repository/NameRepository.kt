package net.juzabel.futureworkshops.domain.repository

import io.reactivex.Observable
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.domain.Article

interface NameRepository {
    fun getName(): Observable<String>
    fun setName(name : String): Observable<String>
    fun removeName() : Observable<String>
}