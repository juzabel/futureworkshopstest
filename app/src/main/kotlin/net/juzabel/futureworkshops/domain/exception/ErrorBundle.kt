package net.juzabel.futureworkshops.domain.exception

/**
 * Created by juzabel on 28/7/17.
 */
interface ErrorBundle {
    fun getException(): Exception

    fun getErrorMessage(): String
}