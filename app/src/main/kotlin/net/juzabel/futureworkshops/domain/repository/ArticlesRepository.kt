package net.juzabel.futureworkshops.domain.repository

import io.reactivex.Observable
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.domain.Article

interface ArticlesRepository {
    fun getArticles(): Observable<List<Article>>
    fun getArticleDetail(id : Long): Observable<ArticleDetail>}