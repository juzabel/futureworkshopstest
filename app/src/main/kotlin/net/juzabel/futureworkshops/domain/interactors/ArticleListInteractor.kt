package net.juzabel.futureworkshops.domain.interactors

import io.reactivex.Observable
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import net.juzabel.futureworkshops.domain.executor.PostExecutionThread
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import javax.inject.Inject

/**
 * Created by juzabel on 29/7/17.
 */
class ArticleListInteractor
@Inject constructor(threadExecutor: ExecutionThread, postExecutionThread: PostExecutionThread,
                                   private val articlesRepository : ArticlesRepository) : UseCase<List<Article>>(threadExecutor, postExecutionThread){


    /**
     * Creates the use case
     *
     * @return Observable<List<Article>>
     */
    override fun buildUseCaseObservable(): Observable<List<Article>>
        = this.articlesRepository.getArticles()



}