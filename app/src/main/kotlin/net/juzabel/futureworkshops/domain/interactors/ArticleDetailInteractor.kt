package net.juzabel.futureworkshops.domain.interactors

import io.reactivex.Observable
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import net.juzabel.futureworkshops.domain.executor.PostExecutionThread
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import javax.inject.Inject

/**
 * Created by juzabel on 29/7/17.
 */
class ArticleDetailInteractor
@Inject constructor(threadExecutor: ExecutionThread, postExecutionThread: PostExecutionThread,
                                   private val articlesRepository: ArticlesRepository) : UseCase<ArticleDetail>(threadExecutor, postExecutionThread){
    var id : Long = -1

    /**
     * Sets the parameters to execute the use case
     *
     * @param id Article identifier
     */
    fun setParameters(id: Long){
        this.id = id
    }

    /**
     * Creates the use case
     *
     * @return Observable<ArticleDetail>
     */
    override fun buildUseCaseObservable(): Observable<ArticleDetail>
        = this.articlesRepository.getArticleDetail(id)



}