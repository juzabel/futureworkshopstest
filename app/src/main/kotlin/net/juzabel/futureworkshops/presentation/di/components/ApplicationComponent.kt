package net.juzabel.futureworkshops.presentation.di.components

import android.content.Context
import android.content.SharedPreferences
import dagger.Component
import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import net.juzabel.futureworkshops.domain.executor.PostExecutionThread
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import net.juzabel.futureworkshops.domain.repository.NameRepository
import net.juzabel.futureworkshops.presentation.FWApplication
import net.juzabel.futureworkshops.presentation.di.modules.ApplicationModule
import net.juzabel.futureworkshops.presentation.navigation.Navigator
import javax.inject.Singleton

/**
 * Created by juzabel on 28/7/17.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(FWApplication: FWApplication)

    val FWApplication: FWApplication

    fun context() : Context

    fun navigator() : Navigator

    fun threadExecutor(): ExecutionThread

    fun postExecutionThread(): PostExecutionThread

    fun sharedPreferences(): SharedPreferences

    fun articlesRepository(): ArticlesRepository

    fun nameRepository(): NameRepository

}