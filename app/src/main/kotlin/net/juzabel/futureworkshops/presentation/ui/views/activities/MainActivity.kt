package net.juzabel.futureworkshops.presentation.ui.views.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import net.juzabel.futureworkshops.R
import net.juzabel.futureworkshops.presentation.FWApplication

/**
 * Main activity of the app, where the fragments are loaded
 */
class MainActivity : AppCompatActivity() {

    companion object {
        const val TOOLBAR_TITLE: String = "TOOLBAR_TITLE"
    }

    private var toolbarTitle: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        //Saving state code, to avoid state loss
        if (savedInstanceState != null) {
            onRestoreState(savedInstanceState)
        } else {
            FWApplication[this].component.navigator().showLogin(this, R.id.fragment_container)
        }
    }

    /**
     * Writes the title into the toolbar (Used by fragments)
     *
     * @param title title text of the toolbar
     *
     */
    override fun setTitle(title: CharSequence?) {
        super.setTitle("")
        this.toolbarTitle = title.toString()
        toolbar_title_text.text = title.toString()
    }

    /**
     * Saving state
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putString(TOOLBAR_TITLE, toolbarTitle)
    }

    /**
     * Restoring state
     */
    fun onRestoreState(inState: Bundle?) {
        toolbarTitle = inState!!.getString(TOOLBAR_TITLE)
        this.setTitle(toolbarTitle)
    }
}
