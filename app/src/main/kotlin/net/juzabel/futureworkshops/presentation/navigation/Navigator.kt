package net.juzabel.futureworkshops.presentation.navigation

import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

import net.juzabel.futureworkshops.R
import net.juzabel.futureworkshops.presentation.ui.views.fragments.ArticlesListFragment
import net.juzabel.futureworkshops.presentation.ui.views.fragments.LoginFragment
import javax.inject.Inject
import android.support.annotation.IntDef
import android.support.annotation.StringDef
import android.support.v4.app.FragmentActivity
import net.juzabel.futureworkshops.presentation.ui.views.fragments.ArticleDetailFragment
import java.lang.annotation.RetentionPolicy


/**
 * Created by juzabel on 29/7/17.
 *
 * Main navigator class to change fragments
 *
 */
class Navigator
@Inject constructor() {

    companion object {
        @StringDef(LOGIN_FRAGMENT, ARTICLE_LIST_FRAGMENT, ARTICLE_DETAIL_FRAGMENT)
        @Retention(AnnotationRetention.SOURCE)
        annotation class Fragments

        const val LOGIN_FRAGMENT: String = "LOGIN_FRAGMENT"
        const val ARTICLE_LIST_FRAGMENT: String = "ARTICLE_LIST_FRAGMENT"
        const val ARTICLE_DETAIL_FRAGMENT: String = "ARTICLE_DETAIL_FRAGMENT"
    }

    /**
     * Loads the login fragment into the corresponding layout container
     *
     * @param activity Container activity
     *
     * @param idContainer the resource id of the container
     *
     */
    fun showLogin(activity: FragmentActivity, idContainer : Int){
        var loginFragment : LoginFragment = LoginFragment.newInstance()

        val fragTransaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        fragTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
        fragTransaction.replace(idContainer, loginFragment, LOGIN_FRAGMENT)
        fragTransaction.commit()

    }

    /**
     * Loads the articles list fragment into the corresponding layout container
     *
     * @param activity Container activity
     *
     * @param idContainer the resource id of the container
     *
     */
    fun showArticlesList(activity: FragmentActivity, idContainer : Int){
        var articlesListFragment : ArticlesListFragment = ArticlesListFragment.newInstance()

        val fragTransaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        fragTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)

        fragTransaction.replace(idContainer, articlesListFragment, ARTICLE_LIST_FRAGMENT)
        fragTransaction.commit()
    }

    /**
     * Loads the article detail fragment into the corresponding layout container
     *
     * @param activity Container activity
     *
     * @param idContainer the resource id of the container
     *
     */
    fun showArticleDetail(activity: FragmentActivity, idContainer : Int, idArticle : Long){
        var articleDetailFragment : ArticleDetailFragment = ArticleDetailFragment.newInstance(idArticle)

        val fragTransaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        fragTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)

        fragTransaction.addToBackStack(ARTICLE_DETAIL_FRAGMENT)
        fragTransaction.replace(idContainer, articleDetailFragment, ARTICLE_DETAIL_FRAGMENT)
        fragTransaction.commit()
    }
}