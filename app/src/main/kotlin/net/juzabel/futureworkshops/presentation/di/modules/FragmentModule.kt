package net.juzabel.futureworkshops.presentation.di.modules

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides
import net.juzabel.futureworkshops.presentation.di.scopes.FragmentScope
import net.juzabel.futureworkshops.presentation.navigation.Navigator

/**
 * Created by juzabel on 28/7/17.
 */
@Module
class FragmentModule(private val baseFragment: Fragment) {

    @Provides
    @FragmentScope
    fun provideFragment(): Fragment = this.baseFragment

}
