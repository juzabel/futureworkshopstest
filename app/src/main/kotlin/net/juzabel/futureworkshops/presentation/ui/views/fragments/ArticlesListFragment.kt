package net.juzabel.futureworkshops.presentation.ui.views.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_article_list.*
import net.juzabel.futureworkshops.R
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.presentation.FWApplication
import net.juzabel.futureworkshops.presentation.di.components.DaggerFragmentComponent
import net.juzabel.futureworkshops.presentation.di.components.FragmentComponent
import net.juzabel.futureworkshops.presentation.di.modules.FragmentModule
import net.juzabel.futureworkshops.presentation.ui.contracts.ArticleListContract
import net.juzabel.futureworkshops.presentation.ui.presenters.ArticleListPresenter
import net.juzabel.futureworkshops.presentation.ui.views.adapters.ArticlesListAdapter
import java.util.*
import javax.inject.Inject


/**
 * Created by juzabel on 30/7/17.
 */
class ArticlesListFragment : Fragment(), ArticleListContract.View, ArticlesListAdapter.OnArticleSelectedListener {

    companion object {
        const val ARTICLES_LIST: String = "ARTICLES_LIST"
        const val NAME: String = "NAME"

        fun newInstance() = ArticlesListFragment()
    }

    @Inject
    lateinit var articleListPresenter: ArticleListPresenter

    private var component: FragmentComponent? = null

    private var articlesList: List<Article> = ArrayList()
    private var name: String = ""

    private lateinit var layoutManager: RecyclerView.LayoutManager

    private var alertDialog: AlertDialog? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        component = DaggerFragmentComponent.builder()
                .applicationComponent((activity.application as FWApplication).component)
                .fragmentModule(FragmentModule(this))
                .build()

        component!!.inject(this)
        articleListPresenter.view = this
        articleListPresenter.create()

        return inflater!!.inflate(R.layout.fragment_article_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_article_list_recyclerview.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(activity)
        fragment_article_list_recyclerview.layoutManager = layoutManager

        swipe_refresh_layout.setOnRefreshListener {
            articleListPresenter.getArticlesList()
        }
        swipe_refresh_layout.isRefreshing = true

        //Avoiding state loss
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState)
        } else {
            articleListPresenter.getName()
            articleListPresenter.getArticlesList()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.articles_list_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.itemId) {
            R.id.menu_item_logout -> {
                showLogoutDialog()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     * Obtaining the list of articles
     *
     * @param articlesList List of articles to show in GUI
     *
     */
    override fun articlesGot(articlesList: List<Article>) {

        if (activity != null && !activity.isDestroyed) {

            this.articlesList = articlesList
            setAdapter()
            swipe_refresh_layout.isRefreshing = false

        }

    }

    /**
     * Obtains the username
     *
     * @param name username
     *
     */
    override fun nameGot(name: String) {

        this.name = name

        if (activity != null && !activity.isDestroyed) {

            activity.title =
                    String.format(getString(R.string.user_feed_title), name)

        }

    }

    /**
     * Sets the articles list into adapter to be shown in the recyclerview
     */
    private fun setAdapter() {

        var articlesListAdapter: ArticlesListAdapter = ArticlesListAdapter(this.articlesList, context)

        articlesListAdapter.onArticleSelectedListener = this

        fragment_article_list_recyclerview.adapter = articlesListAdapter

    }

    /**
     * Shows error in case
     */
    override fun error(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    /**
     * If an article of the recyclerview is selected, we load the detail
     *
     * @param article Article to load into ArticleDetailFragment
     *
     */
    override fun onArticleSelected(article: Article) {

        FWApplication[context].component.navigator().showArticleDetail(activity, R.id.fragment_container, article.id)
    }

    /**
     * Restoring instance
     */
    fun onRestoreInstanceState(inState: Bundle?) {
        articlesList = inState!!.getParcelableArrayList(ARTICLES_LIST)
        name = inState!!.getString(NAME)

        nameGot(name)
        articlesGot(articlesList)
    }

    /**
     * Saving instance
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState!!.putParcelableArrayList(ARTICLES_LIST, articlesList as ArrayList)
        outState.putString(NAME, name)

    }

    /**
     * Avoiding dialog leak
     */
    override fun onPause() {
        super.onPause()
        if (alertDialog != null && alertDialog!!.isShowing) {
            alertDialog!!.dismiss()
        }
    }

    /**
     * shows the corresponding logout dialog
     */
    private fun showLogoutDialog() {
        alertDialog = AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog)
                .setTitle(R.string.logout)
                .setMessage(R.string.logout_message)
                .setPositiveButton(R.string.yes, { _, _ ->
                    articleListPresenter.clearName()
                    FWApplication[context].component
                            .navigator().showLogin(activity, R.id.fragment_container)
                })
                .setNegativeButton(R.string.no, null)
                .show()
    }
}