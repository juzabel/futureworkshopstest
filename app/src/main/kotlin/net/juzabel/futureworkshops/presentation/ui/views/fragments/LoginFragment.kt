package net.juzabel.futureworkshops.presentation.ui.views.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import net.juzabel.futureworkshops.R
import net.juzabel.futureworkshops.presentation.FWApplication
import net.juzabel.futureworkshops.presentation.di.components.DaggerFragmentComponent
import net.juzabel.futureworkshops.presentation.di.components.FragmentComponent
import net.juzabel.futureworkshops.presentation.di.modules.FragmentModule
import kotlinx.android.synthetic.main.fragment_login.*
import net.juzabel.futureworkshops.presentation.ui.Constants
import net.juzabel.futureworkshops.presentation.ui.contracts.LoginContract
import net.juzabel.futureworkshops.presentation.ui.presenters.LoginPresenter
import javax.inject.Inject

/**
 * Created by julian on 31/07/17.
 */
class LoginFragment : Fragment(), LoginContract.View, View.OnClickListener {

    companion object {
        fun newInstance() = LoginFragment()
    }

    @Inject
    lateinit var loginPresenter: LoginPresenter

    private var component: FragmentComponent? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        component = DaggerFragmentComponent.builder()
                .applicationComponent((activity.application as FWApplication).component)
                .fragmentModule(FragmentModule(this))
                .build()

        component!!.inject(this)
        loginPresenter.view = this
        loginPresenter.create()

        activity.title = getString(R.string.app_name)
        return inflater!!.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        root_layout.visibility = View.GONE
        loginPresenter.hasName()

        fragment_login_button.setOnClickListener(this)
        Glide.with(this).load(R.drawable.login_image).into(fragment_login_header_image)

    }

    /**
     * Login button click
     */
    override fun onClick(view: View?) {

        if (!fragment_login_name.text.trim().isEmpty()) {

            loginPresenter.saveName(fragment_login_name.text.trim().toString())

        } else {
            fragment_login_name.error = getString(R.string.empty_error)
        }
    }

    /**
     * If the name is correctly saved into DB, we go to the articles list
     */
    override fun nameSaved() {
        FWApplication[context].component.navigator().showArticlesList(activity, R.id.fragment_container)
    }

    /**
     * Checks if username exists to jump into articles list directly
     */
    override fun existName(existName: Boolean) {

        if (existName) {
            FWApplication[context].component.navigator().showArticlesList(activity, R.id.fragment_container)
        }else {
            root_layout.visibility = View.VISIBLE
        }
    }

}