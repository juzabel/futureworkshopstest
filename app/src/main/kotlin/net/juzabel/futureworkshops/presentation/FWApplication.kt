package net.juzabel.futureworkshops.presentation

import android.app.Application
import android.content.Context
import com.raizlabs.android.dbflow.config.FlowManager
import net.juzabel.futureworkshops.presentation.di.components.ApplicationComponent
import net.juzabel.futureworkshops.presentation.di.components.DaggerApplicationComponent
import net.juzabel.futureworkshops.presentation.di.modules.ApplicationModule

/**
 * Created by juzabel on 2/7/17.
 */
class FWApplication : Application() {
    val component: ApplicationComponent
        get() = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
        FlowManager.init(this)
    }

    companion object {

        operator fun get(context: Context): FWApplication {
            return context.applicationContext as FWApplication
        }
    }
}