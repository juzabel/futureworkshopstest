package net.juzabel.futureworkshops.presentation.di.scopes

import javax.inject.Scope

/**
 * Created by juzabel on 28/7/17.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope
