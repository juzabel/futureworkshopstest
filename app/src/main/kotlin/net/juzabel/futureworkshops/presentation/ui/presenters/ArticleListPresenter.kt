package net.juzabel.futureworkshops.presentation.ui.presenters

import io.reactivex.observers.DisposableObserver
import net.juzabel.futureworkshops.domain.Article
import net.juzabel.futureworkshops.domain.interactors.ArticleListInteractor
import net.juzabel.futureworkshops.domain.interactors.GetNameInteractor
import net.juzabel.futureworkshops.domain.interactors.RemoveNameInteractor
import net.juzabel.futureworkshops.presentation.di.scopes.FragmentScope
import net.juzabel.futureworkshops.presentation.ui.contracts.ArticleListContract
import javax.inject.Inject


/**
 * Created by juzabel on 28/7/17.
 */
@FragmentScope
class ArticleListPresenter @Inject
constructor(private val articleListInteractor: ArticleListInteractor,
            private val getNameInteractor: GetNameInteractor,
            private val removeNameInteractor: RemoveNameInteractor) : ArticleListContract.Presenter {

    var view: ArticleListContract.View? = null

    /**
     * Creates the presenter data if it is necessary
     */
    fun create() {

    }

    /**
     * Executes the interactor to obtain the article list
     */
    override fun getArticlesList() {
        articleListInteractor.execute(articlesObserver())
    }

    /**
     * Executes the interactor to obtain username (if exists)
     */
    override fun getName() {
        getNameInteractor.execute(GetNameObserver())
    }

    /**
     * Executes the interactor to remove the user name
     */
    override fun clearName() {
        removeNameInteractor.execute(RemoveNameObserver())
    }

    /**
     * Observer to receive the articles list result
     */
    private inner class articlesObserver : DisposableObserver<List<Article>>() {

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            if (view != null && e != null && e.message != null) {
                view!!.error(e.message!!)
            }
            e.printStackTrace()

        }

        override fun onNext(articlesList: List<Article>) {
            view!!.articlesGot(articlesList)
        }

    }

    /**
     * Observer to receive the name result
     */
    private inner class GetNameObserver : DisposableObserver<String>() {

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            e.printStackTrace()

        }

        override fun onNext(name: String) {
            view!!.nameGot(name)
        }

    }

    /**
     * Observer to receive the remvoed name result
     */
    private inner class RemoveNameObserver : DisposableObserver<String>() {

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            e.printStackTrace()

        }

        override fun onNext(name: String) {
        }

    }
}