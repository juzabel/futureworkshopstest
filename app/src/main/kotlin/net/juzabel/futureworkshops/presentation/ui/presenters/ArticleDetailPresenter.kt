package net.juzabel.futureworkshops.presentation.ui.presenters

import io.reactivex.observers.DisposableObserver
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.domain.interactors.ArticleDetailInteractor
import net.juzabel.futureworkshops.presentation.ui.contracts.ArticleDetailContract
import javax.inject.Inject

/**
 * Created by juzabel on 28/7/17.
 */
open class ArticleDetailPresenter @Inject constructor(
        var articleDetailInteractor: ArticleDetailInteractor) : ArticleDetailContract.Presenter {

    var view: ArticleDetailContract.View? = null

    /**
     * Creates the presenter data if it is necessary
     */
    fun create() {
    }

    /**
     * Executes the interactor to obtain the article detail
     *
     * @param id article identifier
     *
     */
    override fun getArticleDetail(id: Long) {
        articleDetailInteractor.setParameters(id)
        articleDetailInteractor.execute(ArticleDetailObserver())
    }

    /**
     * Observer to receive the result
     */
    inner class ArticleDetailObserver : DisposableObserver<ArticleDetail>() {

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            view!!.error(e.message!!)
        }

        override fun onNext(articleDetail: ArticleDetail) {
            view!!.articleDetailGot(articleDetail)
        }
    }
}