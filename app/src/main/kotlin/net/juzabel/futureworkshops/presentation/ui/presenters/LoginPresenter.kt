package net.juzabel.futureworkshops.presentation.ui.presenters

import io.reactivex.observers.DisposableObserver
import net.juzabel.futureworkshops.domain.interactors.GetNameInteractor
import net.juzabel.futureworkshops.domain.interactors.SaveNameInteractor
import net.juzabel.futureworkshops.presentation.di.scopes.FragmentScope
import net.juzabel.futureworkshops.presentation.ui.contracts.LoginContract
import javax.inject.Inject


/**
 * Created by juzabel on 28/7/17.
 */
@FragmentScope
class LoginPresenter @Inject
constructor(private val saveNameInteractor: SaveNameInteractor,
            private val getNameInteractor: GetNameInteractor) : LoginContract.Presenter {

    var view: LoginContract.View? = null

    /**
     * Creates the presenter data if it is necessary
     */
    fun create() {

    }

    override fun saveName(name: String) {
        saveNameInteractor.setParameters(name)
        saveNameInteractor.execute(SaveObserver())
    }
    override fun hasName() {
        getNameInteractor.execute(HasNameObserver())
    }


    private inner class SaveObserver : DisposableObserver<String>() {

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            e.printStackTrace()

        }

        override fun onNext(name: String) {
            view!!.nameSaved()
        }

    }
    private inner class HasNameObserver : DisposableObserver<String>() {

        private var name : String? = null

        override fun onComplete() {
            if(name == null){
                view!!.existName(false)
            }
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()

        }

        override fun onNext(name: String) {
            this.name = name
            view!!.existName(true)
        }

    }
}