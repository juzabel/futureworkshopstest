package net.juzabel.futureworkshops.presentation.ui.contracts

import net.juzabel.futureworkshops.data.entity.ArticleDetail

/**
 * Created by juzabel on 28/7/17.
 */
interface ArticleDetailContract {
    interface View {
        fun articleDetailGot(articleDetail : ArticleDetail)
        fun error(message : String)
    }
    interface Presenter {
        fun getArticleDetail(id: Long)
    }
}