package net.juzabel.futureworkshops.presentation.di.components

import dagger.Component
import dagger.Provides
import net.juzabel.futureworkshops.presentation.di.modules.FragmentModule
import net.juzabel.futureworkshops.presentation.di.scopes.FragmentScope
import net.juzabel.futureworkshops.presentation.navigation.Navigator
import net.juzabel.futureworkshops.presentation.ui.views.fragments.ArticlesListFragment
import net.juzabel.futureworkshops.presentation.ui.views.fragments.ArticleDetailFragment
import net.juzabel.futureworkshops.presentation.ui.views.fragments.LoginFragment

/**
 * Created by juzabel on 28/7/17.
 */
@FragmentScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(FragmentModule::class))
interface FragmentComponent {
    fun inject(articlesListFragment: ArticlesListFragment)
    fun inject(articleDetailFragment: ArticleDetailFragment)
    fun inject(loginFragment: LoginFragment)

}