package net.juzabel.futureworkshops.presentation.di.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import net.juzabel.futureworkshops.data.executor.JobExecutor
import net.juzabel.futureworkshops.data.repository.ArticlesDataRepository
import net.juzabel.futureworkshops.data.repository.NameDataRepository
import net.juzabel.futureworkshops.domain.executor.ExecutionThread
import net.juzabel.futureworkshops.domain.executor.PostExecutionThread
import net.juzabel.futureworkshops.domain.repository.ArticlesRepository
import net.juzabel.futureworkshops.domain.repository.NameRepository
import net.juzabel.futureworkshops.presentation.FWApplication
import net.juzabel.futureworkshops.presentation.UIThread
import javax.inject.Singleton

/**
 * Created by juzabel on 28/7/17.
 */
@Module
class ApplicationModule(private val FWApplication: FWApplication) {

    @Provides
    @Singleton
    fun socialImageApplication(): FWApplication
            = FWApplication

    @Provides
    @Singleton
    fun provideContext(): Context
            = FWApplication

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ExecutionThread
            = jobExecutor


    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread
            = uiThread


    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences
            = FWApplication.getSharedPreferences("app", Context.MODE_APPEND)


    @Provides
    @Singleton
    fun provideArticlesDataRepository(articlesDataRepository: ArticlesDataRepository): ArticlesRepository
            = articlesDataRepository


    @Provides
    @Singleton
    fun provideNameDataRepository(nameDataRepository: NameDataRepository): NameRepository
            = nameDataRepository
}