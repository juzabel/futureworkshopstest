package net.juzabel.futureworkshops.presentation.ui.views.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import net.juzabel.futureworkshops.R
import net.juzabel.futureworkshops.domain.Article

/**
 * Created by julian on 28/07/17.
 */
class ArticlesListAdapter constructor(var articlesList: List<Article>
                                      , var context : Context): RecyclerView.Adapter<ArticlesListAdapter.ViewHolder>() {

    var onArticleSelectedListener: OnArticleSelectedListener? = null

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        Glide.with(context)
                .load(articlesList[position].iconUrl)
                .error(R.drawable.placeholder)
                .into(holder!!.articleImage)

        holder.title!!.text = articlesList[position].title
        holder.summary!!.text = articlesList[position].summary
        holder.date!!.text = articlesList[position].date
        holder.articleContainer!!.isClickable = true
        holder.articleContainer!!.setOnClickListener({
            if(onArticleSelectedListener != null){
                onArticleSelectedListener!!.onArticleSelected(articlesList[position])
            }
        })

    }

    override fun getItemCount(): Int = articlesList.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        var view: View = LayoutInflater.from(parent!!.context)
                .inflate(R.layout.adapter_item_article_list, parent, false)

        var viewHolder: ViewHolder = ViewHolder(view)

        return viewHolder
    }

    /**
     * ViewHolder
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var articleContainer: LinearLayout? = null
        var articleImage: ImageView? = null
        var date: TextView? = null
        var title: TextView? = null
        var summary: TextView? = null

        init {
            articleContainer = view.findViewById(R.id.item_container_layout) as LinearLayout?
            articleImage = view.findViewById(R.id.article_image) as ImageView?
            date = view.findViewById(R.id.article_date) as TextView?
            title = view.findViewById(R.id.article_title) as TextView?
            summary = view.findViewById(R.id.article_summary) as TextView?
        }
    }



    /**
     * Listener of click into a row
     */
    interface OnArticleSelectedListener{
        fun onArticleSelected(article:Article)
    }
}