package net.juzabel.futureworkshops.presentation.ui.views.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Toast
import com.bumptech.glide.Glide
import net.juzabel.futureworkshops.R
import net.juzabel.futureworkshops.data.entity.ArticleDetail
import net.juzabel.futureworkshops.presentation.FWApplication
import net.juzabel.futureworkshops.presentation.di.components.DaggerFragmentComponent
import net.juzabel.futureworkshops.presentation.di.components.FragmentComponent
import net.juzabel.futureworkshops.presentation.di.modules.FragmentModule
import net.juzabel.futureworkshops.presentation.ui.contracts.ArticleDetailContract
import net.juzabel.futureworkshops.presentation.ui.presenters.ArticleDetailPresenter
import kotlinx.android.synthetic.main.fragment_article_detail.*
import javax.inject.Inject

/**
 * Created by juzabel on 28/7/17.
 */
class ArticleDetailFragment : Fragment(), ArticleDetailContract.View {

    companion object {

        const val ARTICLE_DETAIL = "ARTICLE_DETAIL"
        const val ID: String = "ID"

        fun newInstance(id: Long): ArticleDetailFragment {
            var articleDetailFragment: ArticleDetailFragment = ArticleDetailFragment()
            var args: Bundle = Bundle()
            args.putLong(ID, id)

            articleDetailFragment.arguments = args

            return articleDetailFragment
        }
    }


    @Inject
    lateinit var articleDetailPresenter: ArticleDetailPresenter

    private var component: FragmentComponent? = null
    private var id: Long = 0

    private var articleDetail: ArticleDetail? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        id = arguments.getLong(ID)

        component = DaggerFragmentComponent.builder()
                .applicationComponent((activity.application as FWApplication).component)
                .fragmentModule(FragmentModule(this))
                .build()

        component!!.inject(this)
        articleDetailPresenter.view = this
        articleDetailPresenter.create()

        activity.title = ""

        return inflater!!.inflate(R.layout.fragment_article_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipe_refresh_layout.isRefreshing = true
        swipe_refresh_layout.setOnRefreshListener {
            articleDetailPresenter.getArticleDetail(id)

        }

        //Avoiding state loss
        if (savedInstanceState != null) {
            onRestoreState(savedInstanceState)
        } else {
            articleDetailPresenter.getArticleDetail(id)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.article_detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun error(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    /**
     * Loads the article data
     *
     * @param articleDetail Article detail object
     *
     */
    override fun articleDetailGot(articleDetail: ArticleDetail) {
        this.articleDetail = articleDetail
        setArticle(this.articleDetail)
    }

    /**
     * Puts the article data into the GUI
     */
    private fun setArticle(articleDetail: ArticleDetail?) {

        if (articleDetail != null && activity != null && !activity.isDestroyed) {

            activity.title = articleDetail.title

            Glide.with(this).load(articleDetail.imageUrl)
                    .error(R.drawable.placeholder)
                    .into(detail_image)

            title_text.text = articleDetail.title
            author_text.text = articleDetail.source
            date_text.text = articleDetail.date

            content_text.text = articleDetail.content

        }
        swipe_refresh_layout.isRefreshing = false

    }

    /**
     * Saving state
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putParcelable(ARTICLE_DETAIL, articleDetail)
    }

    /**
     * Restoring state
     */
    fun onRestoreState(inState: Bundle?) {
        articleDetail = inState!!.getParcelable(ARTICLE_DETAIL)
        setArticle(articleDetail)
    }
}