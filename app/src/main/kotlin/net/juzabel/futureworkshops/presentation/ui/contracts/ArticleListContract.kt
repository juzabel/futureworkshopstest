package net.juzabel.futureworkshops.presentation.ui.contracts

import net.juzabel.futureworkshops.domain.Article

/**
 * Created by juzabel on 28/7/17.
 */
interface ArticleListContract {
    interface View {
        fun articlesGot(articlesList : List<Article>)
        fun error(message : String)
        fun nameGot(name : String)
    }
    interface Presenter {
        fun getArticlesList()
        fun getName()
        fun clearName()
    }
}