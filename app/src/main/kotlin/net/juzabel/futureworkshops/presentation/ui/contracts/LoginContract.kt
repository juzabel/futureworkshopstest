package net.juzabel.futureworkshops.presentation.ui.contracts

/**
 * Created by juzabel on 28/7/17.
 */
interface LoginContract {
    interface View {
        fun nameSaved()
        fun existName(existName : Boolean)
    }
    interface Presenter {
        fun hasName()
        fun saveName(name: String)
    }
}